package car_sharing.entities;

public class Vehicle {
    private Integer vehicle_id;
    private String type;
    private String brand;
    private String model;
    private double coast;

    public Vehicle(String type, String brand, String model, double coast) {
        this.type = type;
        this.brand = brand;
        this.model = model;
        this.coast = coast;
    }

    public Integer getVehicle_id() {
        return vehicle_id;
    }

    public void setVehicle_id(Integer vehicle_id) {
        this.vehicle_id = vehicle_id;
    }

    public Vehicle() {
    }

    public Vehicle(String brand) {
        this.brand = brand;
    }


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public double getCoast() {
        return coast;
    }

    public void setCoast(double coast) {
        this.coast = coast;
    }
}
