package car_sharing.entities;

public class Rent {
    private Integer id_vehicle;
    private Integer id_user;
    private Integer numberOfDaysRent;
    private String time;
    private String date;

    public Rent() {
    }

    public Rent(Integer id_vehicle, Integer id_user, Integer numberOfDaysRent, String time, String date) {
        this.id_vehicle = id_vehicle;
        this.id_user = id_user;
        this.numberOfDaysRent = numberOfDaysRent;
        this.time = time;
        this.date = date;
    }

    public Integer getId_vehicle() {
        return id_vehicle;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setId_vehicle(Integer id_vehicle) {
        this.id_vehicle = id_vehicle;
    }

    public Integer getId_user() {
        return id_user;
    }

    public void setId_user(Integer id_user) {
        this.id_user = id_user;
    }

    public Integer getNumberOfDaysRent() {
        return numberOfDaysRent;
    }

    public void setNumberOfDaysRent(Integer numberOfDaysRent) {
        this.numberOfDaysRent = numberOfDaysRent;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
