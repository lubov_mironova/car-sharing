package car_sharing.test;

import car_sharing.database.Const;
import car_sharing.database.DatabaseHandler;
import car_sharing.entities.Rent;
import car_sharing.entities.User;
import car_sharing.entities.Vehicle;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.sql.ResultSet;
import java.sql.SQLException;

class DatabaseHandlerTest {
    DatabaseHandler databaseHandler = new DatabaseHandler();

    @Test
    void getVehicleCoastByModel() throws SQLException, ClassNotFoundException {

        Vehicle vehicle = new Vehicle();
        String model = "Logan";
        vehicle.setModel(model);
        ResultSet testSet = databaseHandler.getVehicleCoastByModel(vehicle);
        int test = 0;
        while (testSet.next()) {
            test = testSet.getInt(Const.VEHICLE_COAST);
        }
        int actual = 5500;
        Assert.assertEquals(test, actual);
    }

    @Test
    void getUserIdByLogin() throws SQLException, ClassNotFoundException {
        User user = new User();
        String login = "login@example.com";
        user.setLogin(login);
        ResultSet testSet = databaseHandler.getUserIdByLogin(user);
        int test = 0;
        while (testSet.next()) {
            test = testSet.getInt(Const.USER_ID);
        }
        int actual = 3;
        Assert.assertEquals(test, actual);
    }

    @Test
    void getRentVehicleIdAndNumberRentDaysByRentDate() throws SQLException, ClassNotFoundException {
        Rent rent = new Rent();
        String date = "2021-06-18";
        rent.setDate(date);
        ResultSet testSet = databaseHandler.getRentVehicleIdAndNumberRentDaysByRentDate(rent);
        int testId = 0;
        int testDays = 0;
        while (testSet.next()) {
            testId = testSet.getInt(Const.RENT_VEHICLE_ID);
            testDays = testSet.getInt(Const.NUMBER_OF_RENT_DAYS);
        }
        int actualId = 15;
        int actualDays = 5;
        Assert.assertEquals(testId, actualId);
        Assert.assertEquals(testDays, actualDays);
    }
}