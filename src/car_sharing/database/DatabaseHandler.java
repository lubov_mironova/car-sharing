package car_sharing.database;

import car_sharing.entities.Rent;
import car_sharing.entities.User;
import car_sharing.entities.Vehicle;

import java.sql.*;

public class DatabaseHandler extends Configs {
    Connection dbConnection;

    public Connection getDbConnection() throws ClassNotFoundException, SQLException {
        String connection = "jdbc:mysql://" + dbHost + ":" + dbPort + "/" + dbName;
        Class.forName("com.mysql.jdbc.Driver");

        dbConnection = DriverManager.getConnection(connection, dbUser, dbPass);
        return dbConnection;
    }

    public void signUpUser(User user) throws SQLException, ClassNotFoundException {
        String insert = "INSERT INTO " + Const.USER_TABLE + "(" + Const.USER_LASTNAME + "," + Const.USER_NAME + "," +
                Const.USER_MIDDLE_NAME + "," + Const.USER_PHONE_NUMBER + "," + Const.USER_LOGIN + "," + Const.USER_PASSWORD + ")" +
                " VALUES(?,?,?,?,?,?)";
        PreparedStatement prSt = getDbConnection().prepareStatement(insert);
        prSt.setString(1, user.getLastname());
        prSt.setString(2, user.getName());
        prSt.setString(3, user.getMiddleName());
        prSt.setString(4, user.getPhoneNumber());
        prSt.setString(5, user.getLogin());
        prSt.setString(6, user.getPassword());
        prSt.executeUpdate();
    }

    public ResultSet getUser(User user) throws SQLException, ClassNotFoundException {
        ResultSet resSet = null;
        String select = "SELECT * FROM " + Const.USER_TABLE + " WHERE " + Const.USER_LOGIN + "=? AND " + Const.USER_PASSWORD + "=?";
        PreparedStatement prSt = getDbConnection().prepareStatement(select);
        prSt.setString(1, user.getLogin());
        prSt.setString(2, user.getPassword());
        return resSet = prSt.executeQuery();
    }

    public ResultSet getUserIdByLogin(User user) throws SQLException, ClassNotFoundException {
        ResultSet resSet = null;
        String select = "SELECT " + Const.USER_ID + " FROM " + Const.USER_TABLE + " WHERE " + Const.USER_LOGIN + "=?";
        PreparedStatement prSt = getDbConnection().prepareStatement(select);
        prSt.setString(1, user.getLogin());
        return resSet = prSt.executeQuery();
    }

    public ResultSet getUserFullNameAndPhoneByLogin(User user) throws SQLException, ClassNotFoundException {
        ResultSet resSet = null;
        String select = "SELECT " + Const.USER_NAME + ", " + Const.USER_LASTNAME + ", " + Const.USER_MIDDLE_NAME + ", " + Const.USER_PHONE_NUMBER + " FROM " + Const.USER_TABLE + " WHERE " + Const.USER_LOGIN + "=?";
        PreparedStatement prSt = getDbConnection().prepareStatement(select);
        prSt.setString(1, user.getLogin());
        return resSet = prSt.executeQuery();
    }

    public ResultSet getVehicleModelByBrand(Vehicle vehicle) throws SQLException, ClassNotFoundException {
        ResultSet resSet = null;
        String select = "SELECT " + Const.VEHICLE_MODEL + " FROM " + Const.VEHICLE_TABLE + " WHERE " + Const.VEHICLE_BRAND + "=?";
        PreparedStatement prSt = getDbConnection().prepareStatement(select);
        prSt.setString(1, vehicle.getBrand());
        return resSet = prSt.executeQuery();
    }

    public ResultSet getVehicleBrandByType(Vehicle vehicle) throws SQLException, ClassNotFoundException {
        ResultSet resSet = null;
        String select = "SELECT " + Const.VEHICLE_BRAND + " FROM " + Const.VEHICLE_TABLE + " WHERE " + Const.VEHICLE_TYPE + "=?";
        PreparedStatement prSt = getDbConnection().prepareStatement(select);
        prSt.setString(1, vehicle.getType());
        return resSet = prSt.executeQuery();
    }

    public ResultSet getVehicleIdByBrandAndModel(Vehicle vehicle) throws SQLException, ClassNotFoundException {
        ResultSet resSet = null;
        String select = "SELECT " + Const.VEHICLE_ID + " FROM " + Const.VEHICLE_TABLE + " WHERE " + Const.VEHICLE_BRAND + "=? AND " +
                Const.VEHICLE_MODEL + "=?";
        PreparedStatement prSt = getDbConnection().prepareStatement(select);
        prSt.setString(1, vehicle.getBrand());
        prSt.setString(2, vehicle.getModel());
        return resSet = prSt.executeQuery();
    }

    public ResultSet getVehicleCoastByModel(Vehicle vehicle) throws SQLException, ClassNotFoundException {
        ResultSet resSet = null;
        String select = "SELECT " + Const.VEHICLE_COAST + " FROM " + Const.VEHICLE_TABLE + " WHERE " + Const.VEHICLE_MODEL + "=?";
        PreparedStatement prSt = getDbConnection().prepareStatement(select);
        prSt.setString(1, vehicle.getModel());
        return resSet = prSt.executeQuery();
    }

    public void addRent(Rent rent) throws SQLException, ClassNotFoundException {
        String insert = "INSERT INTO " + Const.RENT_TABLE + "(" + Const.RENT_VEHICLE_ID + "," + Const.RENT_USER_ID +
                "," + Const.NUMBER_OF_RENT_DAYS + "," + Const.RENT_TIME + "," + Const.RENT_DATE + ")" + " VALUES(?,?,?,?,?)";
        PreparedStatement prSt = getDbConnection().prepareStatement(insert);
        prSt.setInt(1, rent.getId_vehicle());
        prSt.setInt(2, rent.getId_user());
        prSt.setInt(3, rent.getNumberOfDaysRent());
        prSt.setString(4, rent.getTime());
        prSt.setString(5, rent.getDate());
        prSt.executeUpdate();
    }

    public ResultSet getRentVehicleIdAndNumberRentDaysByRentDate(Rent rent) throws SQLException, ClassNotFoundException {
        ResultSet resSet = null;
        String select = "SELECT " + Const.RENT_VEHICLE_ID + ", " + Const.NUMBER_OF_RENT_DAYS + " FROM " + Const.RENT_TABLE + " WHERE " + Const.RENT_DATE + "=?";
        PreparedStatement prSt = getDbConnection().prepareStatement(select);
        prSt.setString(1, rent.getDate());
        return resSet = prSt.executeQuery();
    }

    public ResultSet getVehicleTypeAndBrandAndModelAndCoastByVehicleId(Vehicle vehicle) throws SQLException, ClassNotFoundException {
        ResultSet resSet = null;
        String select = "SELECT " + Const.VEHICLE_TYPE + ", " + Const.VEHICLE_BRAND + ", " + Const.VEHICLE_MODEL + ", " + Const.VEHICLE_COAST + " FROM " + Const.VEHICLE_TABLE + " WHERE " + Const.VEHICLE_ID + "=?";
        PreparedStatement prSt = getDbConnection().prepareStatement(select);
        prSt.setInt(1, vehicle.getVehicle_id());
        return resSet = prSt.executeQuery();
    }
}
