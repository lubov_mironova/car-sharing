package car_sharing.database;

public class Const {
    public static final String RENT_TABLE = "rent";
    public static final String RENT_ID = "id_rent";
    public static final String RENT_VEHICLE_ID = "rent_id_vehicle";
    public static final String RENT_USER_ID = "rent_id_user";
    public static final String NUMBER_OF_RENT_DAYS = "number_rent_days";
    public static final String RENT_TIME = "rent_time";
    public static final String RENT_DATE = "rent_day";

    public static final String USER_TABLE = "user";
    public static final String USER_ID = "id_user";
    public static final String USER_LASTNAME = "user_lastname";
    public static final String USER_NAME = "user_name";
    public static final String USER_MIDDLE_NAME = "user_middlename";
    public static final String USER_PHONE_NUMBER = "user_phone";
    public static final String USER_LOGIN = "user_login";
    public static final String USER_PASSWORD = "user_password";

    public static final String VEHICLE_TABLE = "vehicle";
    public static final String VEHICLE_ID = "id_vehicle";
    public static final String VEHICLE_TYPE = "vehicle_type";
    public static final String VEHICLE_BRAND = "vehicle_brand";
    public static final String VEHICLE_MODEL = "vehicle_model";
    public static final String VEHICLE_COAST = "vehicle_coast";
}
