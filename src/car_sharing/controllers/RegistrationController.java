package car_sharing.controllers;

import car_sharing.database.DatabaseHandler;
import car_sharing.entities.User;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.SQLException;

public class RegistrationController {

    @FXML
    private TextField nameTextField;

    @FXML
    private TextField lastnameTextField;

    @FXML
    private TextField phoneTextField;

    @FXML
    private TextField signInLoginTextField;

    @FXML
    private TextField middleNameTextField;

    @FXML
    private TextField singInPasswordTextField;

    @FXML
    private Button signInButton;

    @FXML
    private Label signInErrorLabel;

    /**
     * Проверяет заполненность полей для регистрации пользователя
     */
    @FXML
    public void onClickSignIn() throws IOException, ClassNotFoundException {
        if (!lastnameTextField.getText().equals("") && !nameTextField.getText().equals("") && !phoneTextField.getText().equals("") &&
                !signInLoginTextField.getText().equals("") && !singInPasswordTextField.getText().equals("")) {
            signInErrorLabel.setText("");
            singUpNewUser();
            openLogInWindow();
        } else {
            signInErrorLabel.setText("Для регистрации необходимо заполнить все поля*\n*отчество - необязательное поле");
        }
    }

    /**
     * Открывает окно авторизации
     */
    private void openLogInWindow() throws IOException {
        Stage stage = (Stage) signInButton.getScene().getWindow();
        stage.close();
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/car_sharing/views/authorization.fxml"));
        Parent root = fxmlLoader.load();
        stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(new Scene(root));
        stage.setTitle("Авторизация");
        stage.setResizable(false);
        stage.show();
    }

    /**
     * Добавляет нового пользователя в БД
     */
    private void singUpNewUser() throws ClassNotFoundException {
        try {
            DatabaseHandler dbHandler = new DatabaseHandler();
            String lastname = lastnameTextField.getText();
            String name = nameTextField.getText();
            String middleName = middleNameTextField.getText();
            String phone = phoneTextField.getText();
            String login = signInLoginTextField.getText();
            String password = singInPasswordTextField.getText();

            User user = new User(lastname, name, middleName, phone, login, password);
            dbHandler.signUpUser(user);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}