package car_sharing.controllers;

import car_sharing.database.Const;
import car_sharing.database.DatabaseHandler;
import car_sharing.entities.Rent;
import car_sharing.entities.Vehicle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class SettingsController {

    Stage stage;

    @FXML
    private Label successReportLabel;

    @FXML
    private Button exitButton;

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    /**
     * Сохраняет отчет в файл
     */
    public void onClickReportButton() throws ClassNotFoundException {
        try {
            DatabaseHandler dbHandler = new DatabaseHandler();
            DateFormat format = new SimpleDateFormat("yyyy/MM/dd");
            Date newDate = new Date();
            String date = format.format(newDate);
            Rent rent = new Rent();
            Vehicle vehicle = new Vehicle();
            rent.setDate(date);
            ResultSet rentResSet = dbHandler.getRentVehicleIdAndNumberRentDaysByRentDate(rent);
            ArrayList<String> totalReport = new ArrayList<>();
            ArrayList<Double> totalReportVehicleCoast = new ArrayList<>();
            double totalSum = 0;
            while (rentResSet.next()) {
                int idVehicle = rentResSet.getInt(Const.RENT_VEHICLE_ID);
                int numberRentDays = rentResSet.getInt(Const.NUMBER_OF_RENT_DAYS);
                vehicle.setVehicle_id(idVehicle);
                ResultSet vehicleResSet = dbHandler.getVehicleTypeAndBrandAndModelAndCoastByVehicleId(vehicle);
                while (vehicleResSet.next()) {
                    String type = vehicleResSet.getString(Const.VEHICLE_TYPE);
                    String brand = vehicleResSet.getString(Const.VEHICLE_BRAND);
                    String model = vehicleResSet.getString(Const.VEHICLE_MODEL);
                    double vehicleCoast = vehicleResSet.getDouble(Const.VEHICLE_COAST);
                    totalReport.add(type + " " + brand + " " + model + " - " + (numberRentDays * vehicleCoast));
                    totalReportVehicleCoast.add(numberRentDays * vehicleCoast);
                }
            }
            try {
                FileWriter writer = new FileWriter("report.txt");
                for (Double aDouble : totalReportVehicleCoast) {
                    totalSum += aDouble;
                }

                for (String line : totalReport) {
                    writer.write(line);
                    writer.write(System.getProperty("line.separator"));
                }
                writer.write("Итого: " + totalSum);
                writer.close();
                successReportLabel.setText("Отчёт успешно создан");
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Выходит из учетной записи пользователя на окно авторизации
     */
    public void onClickExitButton() throws IOException {
        Stage stages = (Stage) exitButton.getScene().getWindow();
        stages.close();
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/car_sharing/views/authorization.fxml"));
        Parent root = fxmlLoader.load();
        stages = new Stage();
        stages.initModality(Modality.APPLICATION_MODAL);
        stages.setScene(new Scene(root));
        stages.setTitle("Авторизация");
        stages.setResizable(false);
        stages.show();
        stage.close();
    }
}