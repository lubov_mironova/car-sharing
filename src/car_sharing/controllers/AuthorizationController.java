package car_sharing.controllers;

import car_sharing.database.DatabaseHandler;
import car_sharing.entities.User;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AuthorizationController {
    String login;

    @FXML
    private TextField loginTextField;

    @FXML
    private TextField passwordTextField;

    @FXML
    private Button signInButton;

    @FXML
    private Label logInErrorLabel;

    /**
     * Проверяет заполненность полей логина и пароля
     */
    @FXML
    void onClickLogIn() throws IOException, ClassNotFoundException {
        login = loginTextField.getText().trim();
        String password = passwordTextField.getText().trim();
        if (!login.equals("") && !password.equals("")) {
            logInErrorLabel.setText("");
            loginUser(login, password);
        } else {
            logInErrorLabel.setText("Пустое поле логина или пароля");
        }
    }

    /**
     * Ищет пользователя в БД
     *
     * @param login    логин пользователя
     * @param password пароль пользователя
     */
    private void loginUser(String login, String password) throws ClassNotFoundException, IOException {
        try {
            DatabaseHandler dbHandler = new DatabaseHandler();
            User user = new User();
            user.setLogin(login);
            user.setPassword(password);
            ResultSet resSet = dbHandler.getUser(user);
            int counter = 0;
            while (resSet.next()) {
                counter++;
            }
            if (counter >= 1) {
                logInErrorLabel.setText("");
                openMainWindow();
            } else {
                logInErrorLabel.setText("Пользователь не найден");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Открывает главное окно приложения
     */
    private void openMainWindow() throws IOException {
        loginTextField = new TextField();
        Stage stage = (Stage) signInButton.getScene().getWindow();
        stage.close();
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/car_sharing/views/main.fxml"));
        Parent root = fxmlLoader.load();
        stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(new Scene(root));
        stage.setTitle("Каршеринг");
        stage.setResizable(false);
        stage.show();
        MainController mainController = fxmlLoader.getController();
        mainController.setLogin(login);
    }

    /**
     * Открывает окно регистрации
     */
    @FXML
    void onClickRegistration() throws IOException {
        Stage stage = (Stage) signInButton.getScene().getWindow();
        stage.close();
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/car_sharing/views/registration.fxml"));
        Parent root = fxmlLoader.load();
        stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(new Scene(root));
        stage.setTitle("Регистрация");
        stage.setResizable(false);
        stage.show();
    }
}