package car_sharing.controllers;

import car_sharing.database.Const;
import car_sharing.database.DatabaseHandler;
import car_sharing.entities.Rent;
import car_sharing.entities.User;
import car_sharing.entities.Vehicle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;

public class MainController {

    Stage stage;

    String login;

    @FXML
    private Label fullNameLabel;

    @FXML
    private ComboBox<String> choiceBrandComboBox;

    @FXML
    private ComboBox<String> choiceModelComboBox;

    @FXML
    private ComboBox<Integer> choiceNumbersOfDaysComboBox;

    @FXML
    private Button addButton;

    @FXML
    private Label successAddLabel;

    @FXML
    private ComboBox<String> choiceTimeComboBox;

    @FXML
    private TextField costForDayTextField;

    @FXML
    private TextField totalPayableTextField;

    @FXML
    private ComboBox<String> choiceTypeComboBox;

    @FXML
    private Button settingsButton;

    public void setLogin(String login) {
        this.login = login;
    }

    /**
     * Устанавливает изображение на фон кнопки
     */
    private void backgroundImg() {
        BackgroundImage image = new BackgroundImage(new Image("/car_sharing/img/settings.png", 30, 25, false, false), BackgroundRepeat.REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT,
                BackgroundSize.DEFAULT);
        settingsButton.setBackground(new Background(image));
    }

    /**
     * Выводит ФИО пользователя и номер телефона в верхнем правом углу приложения
     */
    @FXML
    void initialize() throws SQLException, ClassNotFoundException {
        backgroundImg();
        DatabaseHandler databaseHandler = new DatabaseHandler();
        User user = new User();
        user.setLogin(login);
        ResultSet resSet = databaseHandler.getUserFullNameAndPhoneByLogin(user);
        while (resSet.next()) {
            String name = resSet.getString(Const.USER_NAME);
            String lastname = resSet.getString(Const.USER_LASTNAME);
            String middleName = resSet.getString(Const.USER_MIDDLE_NAME);
            String prone = resSet.getString(Const.USER_PHONE_NUMBER);
            if (middleName != null) {
                fullNameLabel.setText(lastname + " " + name + " " + middleName + "     тел. " + prone);
            } else {
                fullNameLabel.setText(lastname + " " + name + "     тел. " + prone);
            }
        }
    }

    /**
     * Возвращает идентификатор пользователя
     *
     * @return идентификатор пользователя
     */
    private int getUserId() throws SQLException, ClassNotFoundException {
        DatabaseHandler databaseHandler = new DatabaseHandler();
        User user = new User();
        user.setLogin(login);
        ResultSet resSet = databaseHandler.getUserIdByLogin(user);
        int id = 0;
        while (resSet.next()) {
            id = resSet.getInt(Const.USER_ID);
        }
        return id;
    }

    /**
     * Выводит марки ТС в соответствии с типом ТС
     */
    @FXML
    void onChoiceType() throws SQLException, ClassNotFoundException {
        totalPayableTextField.setText("");
        DatabaseHandler dbHandler = new DatabaseHandler();
        Vehicle vehicle = new Vehicle();

        String type = choiceTypeComboBox.getValue();
        vehicle.setType(type);
        ResultSet typeSet = dbHandler.getVehicleBrandByType(vehicle);
        ObservableList<String> brands = FXCollections.observableArrayList();
        while (typeSet.next()) {
            brands.add(typeSet.getString(Const.VEHICLE_BRAND));
        }
        HashSet hashSet = new HashSet(brands);
        brands.clear();
        brands.addAll(hashSet);
        choiceBrandComboBox.setItems(brands);
        choiceBrandComboBox.setDisable(false);
    }

    /**
     * Добавляет аренду в БД
     */
    @FXML
    void onClickAddButton() throws SQLException, ClassNotFoundException {
        DatabaseHandler dbHandler = new DatabaseHandler();
        String time = choiceTimeComboBox.getValue();
        Integer id_vehicle = returnVehicle();
        Integer numbDayRent = Integer.parseInt(String.valueOf(choiceNumbersOfDaysComboBox.getValue()));
        Integer id_user = getUserId();
        String date = getFormatDate();
        Rent rent = new Rent(id_vehicle, id_user, numbDayRent, time, date);
        dbHandler.addRent(rent);
        successAddLabel.setText("Успешно добавлено");
    }

    /**
     * Выводит модели ТС в соответствии с маркой ТС
     */
    public void onChoiceBrand() throws SQLException, ClassNotFoundException {
        clear();
        DatabaseHandler dbHandler = new DatabaseHandler();
        Vehicle vehicle = new Vehicle();

        String brand = choiceBrandComboBox.getValue();
        vehicle.setBrand(brand);
        ResultSet brandSet = dbHandler.getVehicleModelByBrand(vehicle);
        ObservableList<String> models = FXCollections.observableArrayList();
        while (brandSet.next()) {
            models.add(brandSet.getString(Const.VEHICLE_MODEL));
        }
        choiceModelComboBox.setItems(models);
        choiceModelComboBox.setDisable(false);
    }

    /**
     * Вывод стоимость суточной аренды определенной модели ТС в соответствии с выбором пользователя
     */
    public void onChoiceModel() throws ClassNotFoundException {
        try {
            clear();
            DatabaseHandler dbHandler = new DatabaseHandler();
            Vehicle vehicle = new Vehicle();

            String model = choiceModelComboBox.getValue();
            vehicle.setModel(model);
            ResultSet modelSet = dbHandler.getVehicleCoastByModel(vehicle);
            while (modelSet.next()) {
                costForDayTextField.setText(modelSet.getString(Const.VEHICLE_COAST));
            }
            choiceNumbersOfDaysComboBox.setDisable(false);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Выводит стоимость ТС в соответствии с количеством дней аренды
     */
    public void onChoiceNumbersOfDays() {
        Double num = Double.parseDouble(String.valueOf(choiceNumbersOfDaysComboBox.getValue()));
        Double coast = Double.parseDouble(costForDayTextField.getText());
        totalPayableTextField.setText(String.valueOf(num * coast));
        choiceTimeComboBox.setDisable(false);
    }

    /**
     * Делает кнопку добавления доступной для нажатия
     */
    public void onChoiceTime() {
        addButton.setDisable(false);
    }

    /**
     * Очищает поля вывода данных о стоимости ТС
     */
    private void clear() {
        totalPayableTextField.setText("");
        costForDayTextField.setText("");
        successAddLabel.setText("");
        addButton.setDisable(true);
    }

    /**
     * Возвращает идентификатор ТС в соответствии с выбранной маркой и моделью ТС
     *
     * @return идентификатор ТС
     */
    private int returnVehicle() throws ClassNotFoundException {
        int id = 0;
        try {
            DatabaseHandler dbHandler = new DatabaseHandler();
            Vehicle vehicle = new Vehicle();
            vehicle.setBrand(choiceBrandComboBox.getValue());
            vehicle.setModel(choiceModelComboBox.getValue());
            ResultSet resSet = dbHandler.getVehicleIdByBrandAndModel(vehicle);
            while (resSet.next()) {
                id = resSet.getInt(Const.VEHICLE_ID);
            }
            return id;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return id;
    }

    /**
     * Открывает окно настроек
     */
    public void onClickSettingsButton() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/car_sharing/views/settings.fxml"));
        Parent root = fxmlLoader.load();
        Stage stages = new Stage();
        stages.initModality(Modality.APPLICATION_MODAL);
        stages.setScene(new Scene(root));
        stages.setTitle("Настройки");
        stages.show();
        stages.setResizable(false);
        stage = (Stage) addButton.getScene().getWindow();
        SettingsController settingsController = fxmlLoader.getController();
        settingsController.setStage(stage);
    }

    /**
     * Возвращает дату в формате ГГГГ/ММ/ДД
     *
     * @return дата в формате ГГГГ/ММ/ДД
     */
    private String getFormatDate() {
        DateFormat format = new SimpleDateFormat("yyyy/MM/dd");
        Date date = new Date();
        return format.format(date);
    }
}